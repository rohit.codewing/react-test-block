<?php

/**
 * Main Plugin Class
 *
 * @package         React_Test_Block
 */

 namespace React_Test_block;


 defined( 'ABSPATH' ) || exit;


 /**
  * Final React Test Block Class
  * 
  * @package         React_Test_Block
  */
  final class ReactTestBlock {

    /**
     * Version
     */
    public $version = '1.0.0';

    /**
     * Single Instance of the class
     */
    protected static $_instance = null;

    /**
     * Class Instance
     * 
     * @return class instance
     */

    public static function instance() {
        if ( is_null( self::$_instance ) ){
            self::$_instance = new self();

        }
        return self::$_instance;
    }

    /**
     * Plugin Constructor
     */

    public function __construct(){
        $this->defineConstants();
        $this->includes();
        $this->init_hooks();

        $this->admin_settings = new ReactTestBlockAdmin();
    }

    /**
     * Fires during plugin activatioon
     * 
     * @return void
     */

    public function activate() {

    }

    /**
     * Fires during plugin deactivatioon
     * 
     * @return void
     */

    public function deactivate() {

    }

    /**
     * Init Plugin Hooks
     * 
     * @return void
     */

    public function init_hooks(){
        register_activation_hooks( REACT_TEST_BLOCK_PLUGIN_FILE, [ $this, 'activate' ] );
        register_deactivation_hooks( REACT_TEST_BLOCK_PLUGIN_FILE, [ $this, 'deactivate' ] );


    }

    /**
     * define constants
     * 
     * @return void
     */

    public function defineConstants(){
        $this->define( 'REACT_TEST_BLOCK_PLUGIN_NAME', 'react-test-block' );
        $this->define( 'REACT_TEST_BLOCK_ABSPATH', dirname(REACT_TEST_BLOCK_PLUGIN_FILE ) . '/' );
        $this->define( 'REACT_TEST_BLOCK_VERSION', $this->version );




    }

    /**
     * plugin Inclludes
     * 
     * @return void
     */

    public function includes(){

    }

    /**
     * Define Contstant if not already set
     * 
     * @param string  $name  Constant name.
     * @param string|bool $value Constant value.
     * @return void
     */
    private function define( $name, $value ) {
        if ( !defined( $name ) ) {
            define( $name, $value );
        }
    }


  }