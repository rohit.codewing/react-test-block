<?php

/**
 * Admin Screen function
 *
 * @package         React_Test_Block
 */

 namespace React_Test_block;


 defined( 'ABSPATH' ) || exit;

 
/**
 * admin class
 * @package  React_Test_Block
 */

 class ReactTestBlockAdmin {

    /**
     * Constructor
     */

    public function __construct() {
        $this->init();
    }

    /**
     * Init
     * @return void
     */

     public function init(){

        add_action( 'enqueue_block_editor_assets', [ $this, 'gb_editor_assets' ] );

     }

     /**
      * enqueue Blocks
      * @return void
      */

     public function gb_editor_assets() {

        $blocks_deps = include_once plugin_dir_path( REACT_TEST_BLOCK_PLUGIN_FILE ) . '/app/build/blocks.asset.php';

        wp_enqueue_script( 'react-test-block', plugin_dir_url( REACT_TEST_BLOCK_PLUGIN_FILE ) . '/app/build/block.js', $blocks_deps, $blocks_deps['version'], true ); 

     }

 }