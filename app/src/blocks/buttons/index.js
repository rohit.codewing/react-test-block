import { __ } from "@wordpress/i18n";
import Edit from "./Edit.js";
import Save from "./Save.js";
import blockMeta from "./block.json";

import { registerBlockType } from '@wordpress/blocks';

/**
 * Register: Button Block
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType("react-test-block/buttons", {
	title: __("React Test Buttons", "react-test-block"),
	description: __("Add a customizable button.", "react-test-block"),
	icon: "button",
	category: "react-test-block",
	supports: {
		multiple: true,
	},
	keywords: [
		__("Buttons", "react-test-block"),
		__("React Test Buttons", "react-test-block"),
		__("React Test", "react-test-block")
	],
    attributes: {
		title: {
			type: "string",
			default: __("React Test Buttons", "delicious-recipes"),
		},
	},

    /**
	 * The edit function describes the structure of your block in the context of the editor.
	 * This represents what the editor will render when the block is used.
	 *
	 * The "edit" property must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 */

	edit: ({ attributes, setAttributes }) => {
        return (
			<div {...useBlockProps()}>
				<TextControl
					value={attributes.title}
					onChange={(val) => setAttributes({ title: val })}
				/>
                </div>
		);
	},
	save: function ({ attributes }) {
		return null;
	},
});