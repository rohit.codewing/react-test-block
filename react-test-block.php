<?php
/**
 * Plugin Name:     React Test Block
 * Plugin URI:      PLUGIN SITE HERE
 * Description:     PLUGIN DESCRIPTION HERE
 * Author:          YOUR NAME HERE
 * Author URI:      YOUR SITE HERE
 * Text Domain:     react-test-block
 * Domain Path:     /languages
 * Version:         1.0.0
 *
 * @package         React_Test_Block
 */

use React_Test_block\ReactTestBlock ;

defined( 'ABSPATH' ) || exit;

//include autoloader

require_once __DIR__ .'/vendor/autoload.php';

if ( ! defined( 'REACT_TEST_BLOCK_PLUGIN_FILE' ) ) {
    defined( 'REACT_TEST_BLOCK_PLUGIN_FILE', __FILE__ );
}

// Init Function

function React_Test_Block_init() {
    return ReactTestBlock::instance();
}

$GLOBALS['React_Test_Block'] = React_Test_Block_init();




